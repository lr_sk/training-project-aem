package by.itechart.training.core.services;

import org.apache.log4j.Logger;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;

@Component(service = NodeService.class, immediate = true)
public class NodeService {

    Logger log = Logger.getLogger(NodeService.class);

    @Reference
    private SessionService sessionService;

    public NodeIterator getNodesFromQuery(String userQuery, String queryLanguage) {
        Session session = sessionService.getSessionFromResourceResolver();
        NodeIterator nodes = null;

        if (session != null) {
            try {
                Query query = session.getWorkspace().getQueryManager().createQuery(userQuery, queryLanguage);
                nodes = query.execute().getNodes();
            } catch (RepositoryException e) {
                e.printStackTrace();
                log.error(e.getStackTrace());
            } finally {
                //sessionService.closeSession(session);
            }
        }
        return nodes;
    }
}
