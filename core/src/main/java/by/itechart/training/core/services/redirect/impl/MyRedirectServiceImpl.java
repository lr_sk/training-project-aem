package by.itechart.training.core.services.redirect.impl;

import by.itechart.training.core.services.redirect.MyRedirectService;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;

import java.io.IOException;

@Component(
        service = MyRedirectServiceImpl.class,
        immediate = true)
public class MyRedirectServiceImpl implements MyRedirectService {

    final String redirectToURL = "http://localhost:4502/content/we-retail/us/en.html";

    @Override
    public void redirect(SlingHttpServletResponse response) throws IOException {
        response.sendRedirect(redirectToURL);
    }
}
