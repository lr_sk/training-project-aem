package by.itechart.training.core.services;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import java.util.ArrayList;
import java.util.List;

@Component(service = TextImageButtonService.class, immediate = true)
public class TextImageButtonService {

    @Reference
    NodeService queryHandler;

    private final String weRetailFirstLevelChildXpath = "/jcr:root/content/we-retail/element(*, cq:Page)";
    private final String weRetailFirstLevelChildSql2 = "SELECT * FROM [cq:Page] AS page WHERE ISCHILDNODE(page ,\"/content/we-retail/\")";

    public String censoringText(String text, String invalidText, String maskTemplate) {
        String censoredText = new String(new char[invalidText.length()]).replace("\0", maskTemplate);
        return text.replace(invalidText, censoredText);
    }


    public NodeIterator getWeRetailSubPages(String queryLanguage) {
        NodeIterator pages = null;
        switch (queryLanguage.toLowerCase()) {
            case "xpath":
                pages = queryHandler.getNodesFromQuery(weRetailFirstLevelChildXpath, Query.XPATH);
                break;
            case "sql2":
                pages = queryHandler.getNodesFromQuery(weRetailFirstLevelChildSql2, Query.JCR_SQL2);
                break;
        }
        return pages;
    }

    public List<String> getNodesNameList (NodeIterator nodes) {

        List<String> namesList = new ArrayList<>();

        if (nodes != null) {
            while (nodes.hasNext()) {
                Node node = nodes.nextNode();
                try {
                    namesList.add(node.getName());
                } catch (RepositoryException e) {
                    e.printStackTrace();
                }
            }
        }

        return namesList;
    }
}
