package by.itechart.training.core.servlets;

import by.itechart.training.core.services.redirect.impl.MyRedirectServiceImpl;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.servlets.annotations.SlingServletResourceTypes;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;

@Component(service = { Servlet.class })
@SlingServletResourceTypes(
        resourceTypes="training-project-skomorokhova/components/helloworld",
        methods= HttpConstants.METHOD_GET,
        extensions = "html")

public class MyRedirectServlet extends SlingSafeMethodsServlet {

    @Reference
    private MyRedirectServiceImpl redirectService;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        redirectService.redirect(response);
    }
}
