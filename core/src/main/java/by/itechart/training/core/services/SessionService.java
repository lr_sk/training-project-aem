package by.itechart.training.core.services;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.Collections;
import java.util.Map;

@Component(service = SessionService.class, immediate = true)
public class SessionService {

    private final Logger log = LoggerFactory.getLogger(SessionService.class);
    private final String serviceUserName = "getResourceResolver";

    @Reference
    private transient ResourceResolverFactory resourceResolverFactory;

    @Reference
    private SlingRepository slingRepository;

    private ResourceResolver getResourceResolver() {
        ResourceResolver resourceResolver = null;

        try {
            final Map<String, Object > param = Collections.singletonMap(ResourceResolverFactory.SUBSERVICE, (Object) serviceUserName);
            resourceResolver = resourceResolverFactory.getServiceResourceResolver(param);
        } catch (LoginException e) {
            e.printStackTrace();
            log.error("Error stackTrace: " + e.getStackTrace());
        }
        return resourceResolver;
    }

    public Session getSessionFromResourceResolver() {
        ResourceResolver resourceResolver = getResourceResolver();
        Session session = null;
        if (resourceResolver != null) {
            session = resourceResolver.adaptTo(Session.class);
        }
        return session;
    }


    public Session getSessionFromSlingRepository() throws RepositoryException {
        return slingRepository.loginService(serviceUserName, null);
    }

    public void closeSession(Session session) {
        if (session != null) {
            session.logout();
        }
    }
}
