package by.itechart.training.core.services.redirect;

import org.apache.sling.api.SlingHttpServletResponse;

import java.io.IOException;

public interface MyRedirectService {
    void redirect(SlingHttpServletResponse response) throws IOException;
}
