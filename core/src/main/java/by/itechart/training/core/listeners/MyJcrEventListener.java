package by.itechart.training.core.listeners;

import by.itechart.training.core.services.SessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;

import javax.jcr.*;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;

@Component(service = EventListener.class, immediate = true)
public class MyJcrEventListener implements EventListener {

    @Reference
    private SlingRepository slingRepository;

    @Reference
    private SessionService sessionService;

    private Session session;
    private Workspace workspace;

    private static final Logger log = LoggerFactory.getLogger(MyJcrEventListener.class);

    private final String projectPath = "/content/training-project-skomorokhova";
    private final String homePagePath = projectPath + "/language-masters/en/home-page/jcr:content/root/container/container";
    private final String backupPath = projectPath + "/backup/textImageButtonBackup";
    private final int events = Event.NODE_REMOVED | Event.NODE_ADDED | Event.PROPERTY_CHANGED;


    @Activate
    public void activate(ComponentContext context) {

        try {
            this.session = sessionService.getSessionFromSlingRepository();

            if (this.session != null) {
                workspace = session.getWorkspace();
                workspace.getObservationManager().addEventListener(
                        this,
                        events,
                        homePagePath,
                        true,
                        null,
                        null,
                        false
                );
            }
        } catch (RepositoryException e) {
            log.error("Catching error in activate - " + e.getMessage());
        }
    }

    @Override
    public void onEvent(EventIterator eventIterator) {

        while (eventIterator.hasNext()) {

            try {
                Event event = eventIterator.nextEvent();

                if (event != null) {
                    if (event.getType() == Event.NODE_REMOVED) {
                        log.info("Removed node " + event.getIdentifier());
                    } else {
                        String path = event.getIdentifier();
                        String destPath = backupPath + path.substring(path.lastIndexOf('/'));

                        if (session.nodeExists(destPath)) {
                            session.removeItem(destPath);
                            session.save();
                        }
                        session.getWorkspace().copy(path, destPath);
                        log.info("Created backup copy of " + event.getIdentifier());
                    }
                }
            } catch (RepositoryException e) {
                log.error("Catching error on event - " + e.getMessage());
            }
        }
    }

    @Deactivate
    public void deactivate() {
        sessionService.closeSession(this.session);
    }

}
