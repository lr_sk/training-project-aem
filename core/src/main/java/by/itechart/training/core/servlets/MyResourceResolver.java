package by.itechart.training.core.servlets;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.caconfig.annotation.Property;
import org.apache.sling.servlets.annotations.SlingServletPaths;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

@Component(service = { Servlet.class })
@SlingServletPaths({ "/bin/resource" })
public class MyResourceResolver extends SlingSafeMethodsServlet {

    @Reference
    private transient ResourceResolverFactory resourceResolverFactory;

    public void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        try {
            final Map<String, Object > param = Collections.singletonMap(ResourceResolverFactory.SUBSERVICE, (Object) "getResourceResolver");
            ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver(param);
            String path = resourceResolver.getResource("/apps/sling").getPath();
            response.getWriter().write(path);
        } catch (LoginException e) {
            e.printStackTrace();
            response.getWriter().write(e.getMessage());
        }
    }
}