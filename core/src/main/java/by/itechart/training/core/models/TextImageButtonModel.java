package by.itechart.training.core.models;

import by.itechart.training.core.services.TextImageButtonService;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.NodeIterator;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TextImageButtonModel {

    private List<String> weRetailChildPagesNames = new ArrayList<>();

    @Inject
    TextImageButtonService service;

    @Inject
    private String alertMessage;

    @Inject
    private String buttonLabel;

    @Inject
    private String fileReference;

    @Inject
    private String linkTo;

    @Inject
    private String titleText;

    @Inject
    private String queryLanguage;

    @PostConstruct
    protected void init() {
        if (titleText != null) {
            titleText = service.censoringText(titleText, "бутылка", "*");
        }

//        if(queryLanguage != null) {
//            NodeIterator nodes = service.getWeRetailSubPages(queryLanguage);
//            weRetailChildPagesNames = service.getNodesNameList(nodes);
//        }
    }

    public String getAlertMessage() {
        return alertMessage;
    }

    public String getButtonLabel() {
        return buttonLabel;
    }

    public String getFileReference() {
        return fileReference;
    }

    public String getLinkTo() {
        return linkTo;
    }

    public String getTitleText() {
        return titleText;
    }

    public String getQueryLanguage() {
        return queryLanguage;
    }

    public List<String> getWeRetailChildPagesNames() {
        return weRetailChildPagesNames;
    }
}
